package com.mono.mandrill.fitnessgymapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.mono.mandrill.fitnessgymapp.dao.EvaluacionDao;
import com.mono.mandrill.fitnessgymapp.dao.EvaluacionDaoImpl;
import com.mono.mandrill.fitnessgymapp.model.Evaluacion;

import java.util.Calendar;

public class EvaluationActivity extends AppCompatActivity {

    EditText fecha, peso, estatura, imc;

    Button ingresar, calcular;

    int dia, mes, anio;

    String _username, _estatura;

    EvaluacionDao evalDao = new EvaluacionDaoImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluation);

        _username = getIntent().getStringExtra("username");
        _estatura = getIntent().getStringExtra( "estatura");

        fecha = (EditText) findViewById(R.id.txtFecha);
        peso = (EditText) findViewById(R.id.txtPeso);
        estatura = (EditText) findViewById(R.id.txtEstatura);
        imc = (EditText) findViewById(R.id.txtImc);

        estatura.setText(_estatura.trim());

        ingresar = (Button) findViewById(R.id.btnIngresar);
        calcular = (Button) findViewById(R.id.btnCalcular);

        fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seleccionFecha();
            }
        });

        ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validarDatos();
            }
        });

        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcularImc();
            }
        });
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
    }

    public void seleccionFecha(){
        final Calendar cal = Calendar.getInstance();
        dia = cal.get(Calendar.DAY_OF_MONTH);
        mes = cal.get(Calendar.MONTH);
        anio = cal.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(EvaluationActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                fecha.setText(y + "-" + String.valueOf(100 + (m+1)).substring(1) + "-" + String.valueOf(100 + d).substring(1));
            }
        }, anio, mes, dia);

        datePickerDialog.show();
    }

    public void validarDatos(){
        if (fecha.getText().toString().trim().equalsIgnoreCase("")){
            fecha.setError("Debe ingresar fecha");
        } else if(peso.getText().toString().trim().equalsIgnoreCase("")){
            peso.setError("Debe ingresar peso");
        } else if(estatura.getText().toString().trim().equalsIgnoreCase("")){
            estatura.setError("Debe ingresar estatura");
        } else if(imc.getText().toString().trim().equalsIgnoreCase("")) {
            peso.setError("Debe hacer calculo de I.M.C.");
        } else {
            Evaluacion _ev = new Evaluacion();
            _ev.setPeso(Float.parseFloat(peso.getText().toString().trim()));
            _ev.setFecha(fecha.getText().toString().trim());
            _ev.setEmail(_username);
            _ev.setImc(Float.parseFloat(imc.getText().toString().trim().replace(",",".")));
            guardar(_ev);
        }
    }

    private void guardar(Evaluacion ev) {
        long res = evalDao.insertar(ev);

        if (res > 0){
            Toast.makeText(this, "Evaluación guardada correctamente", Toast.LENGTH_LONG);
            onBackPressed();
        } else {
            Toast.makeText(this, "Hubo un error al guardar Evaluación", Toast.LENGTH_LONG);
        }
    }

    public void calcularImc(){
        if(peso.getText().toString().trim().equalsIgnoreCase("")) {
            peso.setError("Debe ingresar peso");
        } else if(peso.getText().toString().trim().length()==0){
            peso.setError("Debe ingresar peso");
        } else if(estatura.getText().toString().trim().equalsIgnoreCase("")){
            estatura.setError("Debe ingresar estatura");
        }
        double _peso = Double.parseDouble(peso.getText().toString().trim());
        double _estatura = Double.parseDouble(estatura.getText().toString().trim());

        try {
            double _imc = Math.round((_peso / (_estatura * _estatura)) * 10000);
            imc.setText(String.format("%.2f", _imc));
        } catch (Exception e){
            
        }

        peso.setError(null);
    }
}
