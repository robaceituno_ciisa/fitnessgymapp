package com.mono.mandrill.fitnessgymapp;

import androidx.activity.OnBackPressedDispatcherOwner;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mono.mandrill.fitnessgymapp.dao.UsuarioDao;
import com.mono.mandrill.fitnessgymapp.dao.UsuarioDaoImpl;
import com.mono.mandrill.fitnessgymapp.model.Usuario;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button launchRegistro;
    Button launchBusqueda;
    FloatingActionButton guardar;

    EditText email, nombre, apellido, fechaNac, estatura, contrasegna, contrasegna2;

    int dia, mes, anio;

    String username = null;

    UsuarioDao usuarioDao = new UsuarioDaoImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("DBHelper",  ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + this.getDatabasePath("db").getAbsolutePath().toString());

        email = (EditText) findViewById(R.id.txtCorreoUsuario);
        nombre = (EditText) findViewById(R.id.txtNombreUsuario);
        apellido = (EditText) findViewById(R.id.txtApellidoUsuario);
        fechaNac = (EditText) findViewById(R.id.txtNacimiento);
        estatura = (EditText) findViewById(R.id.txtEstatura);
        contrasegna = (EditText) findViewById(R.id.txtContrasena);
        contrasegna2 = (EditText) findViewById(R.id.txtContrasena2);

        username = getIntent().getStringExtra("username");

        Log.d("Main", "Username::" + username);
        //---- busca datos
        if (username!=null){
            Usuario _us = buscar(username);
            if (_us!=null) {
                email.setText(_us.getEmail());
                nombre.setText(_us.getNombre());
                apellido.setText(_us.getApellido());
                fechaNac.setText(_us.getFechaNac());
                estatura.setText(Integer.toString(_us.getEstatura()));
            } else {
                email.setText(username);
            }
        }
        // ---- Botones

        fechaNac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar cal = Calendar.getInstance();
                dia = cal.get(Calendar.DAY_OF_MONTH);
                mes = cal.get(Calendar.MONTH);
                anio = cal.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        fechaNac.setText(y + "-" + String.valueOf(100+m+1).substring(1) + "-" + String.valueOf(100+d).substring(1));
                    }
                }, anio, mes, dia);

                datePickerDialog.show();
            }
        });

        launchRegistro = (Button) findViewById(R.id.btnRegistroEvas);

        launchRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, EvaluationActivity.class);
                intent.putExtra("username", email.getText().toString());
                intent.putExtra("estatura", estatura.getText().toString());
                startActivity(intent);
            }
        });

        launchBusqueda = (Button) findViewById(R.id.btnBusqueda);

        launchBusqueda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.putExtra("username", email.getText().toString());
                startActivity(intent);
            }
        });

        guardar =  (FloatingActionButton) findViewById(R.id.btnGuardar);

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Usuario _u = new Usuario();
                Log.d("Main","Click en Guardar");
                try{
                    String pass1 = contrasegna.getText().toString();
                    String pass2 = contrasegna2.getText().toString();

                    if (pass1=="" || pass1.isEmpty()){
                        Log.d("Main", "Contraseña vacia");
                        Toast.makeText(MainActivity.this, "Contraseña no puede estar vacia", Toast.LENGTH_LONG).show();
                    } else if (!pass1.trim().equals(pass2.trim())){
                        Log.d("Main", "Contraseñas diferentes");
                        Toast.makeText(MainActivity.this, "Las contraseñas deben ser iguales", Toast.LENGTH_LONG).show();
                    } else
                        _u.setEmail(email.getText().toString());
                        _u.setNombre(nombre.getText().toString());
                        _u.setApellido(apellido.getText().toString());
                        _u.setFechaNac(fechaNac.getText().toString());
                        _u.setEstatura(Integer.parseInt(estatura.getText().toString()));
                        _u.setContrasegna(contrasegna.getText().toString());
                        guardar(_u);
                } catch (Exception e){
                    Log.d("Error", e.getMessage());
                    Toast.makeText(MainActivity.this, "Error en parseo de datos, revise", Toast.LENGTH_LONG).show();
                }
            }
        });

    }


    public void updateDAO2FE(Usuario _u){

    }

    public Usuario buscar(String username){
        List<Usuario> _u = usuarioDao.buscarPorEmail(username);
        if (_u.size()>0){
            return _u.get(0);
        }
        return null;
    }

    public void guardar(Usuario _usuario){

        Log.d("Main", "Vamos a guardar");

        long res = usuarioDao.insertar( _usuario);

        if (res == 1) {
            Log.d("Main", "Grabado correctamente");
            Toast.makeText(MainActivity.this, "Grabado correctamente", Toast.LENGTH_LONG).show();
        } else {
            Log.d("Main", "Usuario ya existe");
            //Toast.makeText(MainActivity.this, "Error en guardar datos", Toast.LENGTH_LONG).show();

            long act = usuarioDao.actualizar(_usuario);
            if (act == 1) {
                Log.d("Main", "Actualizado correctamente");
                Toast.makeText(MainActivity.this, "Actualizado correctamente", Toast.LENGTH_LONG).show();
            } else {
                Log.d("Main", "No se pudo actualizar");
                Toast.makeText(MainActivity.this, "No se pudo actualizar usuario", Toast.LENGTH_LONG).show();
            }
        }

    }


}

