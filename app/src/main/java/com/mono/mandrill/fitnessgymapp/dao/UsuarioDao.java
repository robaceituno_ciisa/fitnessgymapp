package com.mono.mandrill.fitnessgymapp.dao;

import android.content.Context;

import com.mono.mandrill.fitnessgymapp.model.Usuario;

import java.util.List;

public interface UsuarioDao {

    public long insertar(Usuario _usuario);
    public long actualizar(Usuario _usuario);
    public List<Usuario> buscarPorEmail(String _email);
}
