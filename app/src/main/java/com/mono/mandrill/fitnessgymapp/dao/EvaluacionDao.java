package com.mono.mandrill.fitnessgymapp.dao;

import android.content.Context;

import com.mono.mandrill.fitnessgymapp.model.Evaluacion;

import java.util.ArrayList;
import java.util.List;

public interface EvaluacionDao {

    public long insertar(Evaluacion _evaluacion);
    public List<Evaluacion> buscarPorEmailEntreFechas(String _email, String _inicio, String _termino);
    public long borrarPorEmailFecha(String _email, String fecha);
}
