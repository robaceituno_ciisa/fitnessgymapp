package com.mono.mandrill.fitnessgymapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mono.mandrill.fitnessgymapp.dao.EvaluacionDao;
import com.mono.mandrill.fitnessgymapp.dao.EvaluacionDaoImpl;

public class DetailEvaluation extends AppCompatActivity {

    private static final String TAG = "SearchActivity";

    String detalle, email;
    EditText fecha, peso, imc;
    Button borrar;

    EvaluacionDao evaDao = new EvaluacionDaoImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_evaluation);



        email = getIntent().getStringExtra("username");
        detalle = getIntent().getStringExtra("detalle");

        fecha = (EditText) findViewById(R.id.txtViewFecha);
        peso = (EditText) findViewById(R.id.txtViewPeso);
        imc = (EditText) findViewById(R.id.txtViewImc);

        String[] det = detalle.split("\t\t");

        fecha.setText(det[0]);
        peso.setText(det[1]);
        imc.setText(det[2]);

        borrar = (Button) findViewById(R.id.btnBorrarDetalle);

        borrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                borrar(email, fecha.getText().toString());
            }
        });
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
    }

    public void borrar(String email, String fecha){
        Log.d("Detalle", "Borrando detalle " + email + " " +fecha);
        long res = evaDao.borrarPorEmailFecha(email, fecha);

        if (res>0) {
            Toast.makeText(this, "Se ha eliminado el registro", Toast.LENGTH_LONG);
            onBackPressed();
        } else {
            Toast.makeText(this, "Error al borrar el registro ... reintente", Toast.LENGTH_LONG);
        }


    }
}

