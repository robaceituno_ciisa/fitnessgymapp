package com.mono.mandrill.fitnessgymapp.model;

public class Evaluacion {

    private String email;
    private String fecha;
    private float peso;
    private float imc;

    public Evaluacion(String email, String fecha, float peso, float imc) {
        this.fecha = fecha;
        this.peso = peso;
        this.imc = imc;
    }

    public Evaluacion() {

    }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public double getImc() {
        return imc;
    }

    public void setImc(float imc) {
        this.imc = imc;
    }

}
