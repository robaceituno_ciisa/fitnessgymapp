package com.mono.mandrill.fitnessgymapp;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GymApp extends Application {

    private static Context context;
    private static String serverHostname;
    private static ObjectMapper oMapper;
    private static StrictMode.ThreadPolicy policy;


    public void onCreate() {
        super.onCreate();
        GymApp.context = getApplicationContext();
        GymApp.serverHostname = "192.168.1.89:8888";
        GymApp.oMapper =  new ObjectMapper();
        GymApp.policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    }

    public static Context getAppContext() {
        return GymApp.context;
    }

    public static String getServerHostname() {
        return GymApp.serverHostname;
    }

    public static ObjectMapper getObjectMapper() { return GymApp.oMapper;}

    public static StrictMode.ThreadPolicy getPolicy() { return policy; }
}
