package com.mono.mandrill.fitnessgymapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mono.mandrill.fitnessgymapp.dao.EvaluacionDao;
import com.mono.mandrill.fitnessgymapp.dao.EvaluacionDaoImpl;
import com.mono.mandrill.fitnessgymapp.model.Evaluacion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = "SearchActivity";

    String email;
    TextView inicio;
    TextView termino;
    Button buscar;
    ListView seleccion;
    int iniDia, iniMes, iniAnio, finDia, finMes, finAnio;

    String pattern = "^([0-9]{4}[-/]?((0[13-9]|1[012])[-/]?" +
            "(0[1-9]|[12][0-9]|30)|(0[13578]|1[02])[-/]?31|02[-/]?" +
            "(0[1-9]|1[0-9]|2[0-8]))|([0-9]{2}(([2468][048]|[02468][48])|[13579][26])" +
            "|([13579][26]|[02468][048]|0[0-9]|1[0-6])00)[-/]?02[-/]?29)";
    Pattern r = Pattern.compile(pattern);

    EvaluacionDao evalDao = new EvaluacionDaoImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        email = getIntent().getStringExtra("username");

        inicio = (TextView) findViewById(R.id.txtInicio);
        termino = (TextView) findViewById(R.id.txtTermino);
        buscar = (Button) findViewById(R.id.btnBuscar);
        seleccion = (ListView) findViewById(R.id.listResult);

        inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar cal = Calendar.getInstance();
                iniDia = cal.get(Calendar.DAY_OF_MONTH);
                iniMes = cal.get(Calendar.MONTH);
                iniAnio = cal.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(SearchActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        inicio.setText(y + "-" + String.valueOf(100+(m+1)).substring(1) + "-" + String.valueOf(100+d).substring(1));
                    }
                }, iniAnio, iniMes, iniDia);

                datePickerDialog.show();
            }
        });

        termino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar cal = Calendar.getInstance();
                finDia = cal.get(Calendar.DAY_OF_MONTH);
                finMes = cal.get(Calendar.MONTH);
                finAnio = cal.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(SearchActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        termino.setText(y + "-" + String.valueOf(100+m+1).substring(1) + "-" + String.valueOf(100+d).substring(1));
                    }
                }, finAnio, finMes, finDia);

                datePickerDialog.show();
            }
        });

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validarDatos();
            }
        });

        seleccion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d( TAG, "creando dialogo *******");
                Log.d(TAG, (String) seleccion.getItemAtPosition(i));

                verDetalle((String) seleccion.getItemAtPosition(i));

            }
        });

        inicio.setOnFocusChangeListener(new AdapterView.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus){
                    validarEntreFechas();
                }
            }
        });



    }


    public void verDetalle(String detalle){
        Intent intent = new Intent(this, DetailEvaluation.class);
        intent.putExtra("username", email);
        intent.putExtra("detalle", detalle);
        startActivity(intent);
    }

    public void validarDatos(){
        if (inicio.getText().toString().trim().equalsIgnoreCase("")){
            inicio.setError("Debe ingresar fecha de inicio");
        } else if (termino.getText().toString().trim().equalsIgnoreCase("")){
            termino.setError("Debe ingresar fecha de termino");
        }

        validarEntreFechas();
    }

    public void validarEntreFechas(){
        if (!inicio.getText().toString().trim().equalsIgnoreCase("") && !termino.getText().toString().trim().equalsIgnoreCase("")) {

            String _ini = inicio.getText().toString().trim().replaceAll("-", "");
            String _fin = termino.getText().toString().trim().replaceAll("-", "");

            if (Long.parseLong(_ini) > Long.parseLong(_fin)) {
                inicio.setError("Fecha de inicio de busqueda no puede ser mayor a la fecha de termino");
            } else if (!r.matcher(_ini).find()){
                inicio.setError("Fecha de inicio no es valida");
            } else if (!r.matcher(_fin).find()){
                termino.setError("Fecha de termino no es valida");
            } else {
                inicio.setError(null);
                termino.setError(null);
                ArrayList<String> resultado = new ArrayList<>();

                List<Evaluacion> listado = evalDao.buscarPorEmailEntreFechas(email,
                                                    inicio.getText().toString().trim(),
                                                    termino.getText().toString().trim());

                Toast.makeText(this, "Se han encontrado " + listado.size() + " registros", Toast.LENGTH_LONG);
                for(Evaluacion eva:listado) {
                    resultado.add(eva.getFecha() + "\t\t" + eva.getPeso() + "\t\t" + eva.getImc());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, resultado);
                seleccion.setAdapter(adapter);


            }
        }
    }
}