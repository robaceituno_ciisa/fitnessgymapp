package com.mono.mandrill.fitnessgymapp.data;

import android.annotation.SuppressLint;
import android.os.Build;
import android.system.ErrnoException;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.mono.mandrill.fitnessgymapp.GymApp;
import com.mono.mandrill.fitnessgymapp.MainActivity;
import com.mono.mandrill.fitnessgymapp.R;
import com.mono.mandrill.fitnessgymapp.dao.UsuarioDao;
import com.mono.mandrill.fitnessgymapp.dao.UsuarioDaoImpl;
import com.mono.mandrill.fitnessgymapp.data.model.LoggedInUser;
import com.mono.mandrill.fitnessgymapp.model.Usuario;
import com.mono.mandrill.fitnessgymapp.ui.login.LoginActivity;

import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    UsuarioDao user = new UsuarioDaoImpl();

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication
            List<Usuario> _user = user.buscarPorEmail(username);


            if (_user.size()==0) {
                LoggedInUser newUser =
                        new LoggedInUser(
                                username,
                                "Usuario nuevo");
                return new Result.Success<>(newUser);
            }

            if (!_user.get(0).getContrasegna().trim().equals(password.trim())){
                return new Result.Error(new Exception("Usuario o contraseña incorrecta"));
            }

            LoggedInUser finalUser =
                    new LoggedInUser(
                            _user.get(0).getContrasegna().trim(),
                            _user.get(0).getNombre().trim());
            return new Result.Success<>(finalUser);
        } catch (Exception e) {
            Log.d("Login", e.getMessage());
            return new Result.Error(new IOException("Error de login", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}
