package com.mono.mandrill.fitnessgymapp.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;
import android.system.ErrnoException;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mono.mandrill.fitnessgymapp.GymApp;
import com.mono.mandrill.fitnessgymapp.MainActivity;
import com.mono.mandrill.fitnessgymapp.model.Usuario;
import com.mono.mandrill.fitnessgymapp.ui.login.LoginActivity;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UsuarioDaoImpl implements UsuarioDao{

    String wsUrl = "http://" + GymApp.getServerHostname() + "/users";
    long res;


    @Override
    public long insertar( Usuario _usuario) {
        res = 0;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();
        map.put("Content-Type", "application/json");

        headers.setAll(map);

        Map<String, Object> payload =new HashMap<>();

        payload.put("email", _usuario.getEmail());
        payload.put("nombre", _usuario.getNombre());
        payload.put("apellido", _usuario.getApellido());
        payload.put("estatura", _usuario.getEstatura());
        payload.put("fechaNac", _usuario.getFechaNac());
        payload.put( "contrasegna", _usuario.getContrasegna());

        HttpEntity<?> request = null;
        try {
            request = new HttpEntity<>(GymApp.getObjectMapper().writeValueAsString(payload), headers);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            ResponseEntity<?> response = restTemplate.postForEntity(wsUrl, request, String.class);

            if (response.getBody().equals("ok")){
                res = 1;
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return res;
    }

    @Override
    public long actualizar(Usuario _usuario) {
        res = 0;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();
        map.put("Content-Type", "application/json");

        headers.setAll(map);

        Map<String, Object> payload =new HashMap<>();

        payload.put("email", _usuario.getEmail());
        payload.put("nombre", _usuario.getNombre());
        payload.put("apellido", _usuario.getApellido());
        payload.put("estatura", _usuario.getEstatura());
        payload.put("fechaNac", _usuario.getFechaNac());
        payload.put( "contrasegna", _usuario.getContrasegna());

        HttpEntity<?> request = null;
        try {
            request = new HttpEntity<>(GymApp.getObjectMapper().writeValueAsString(payload), headers);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            ResponseEntity<?> response = restTemplate.postForEntity(wsUrl, request, String.class);

            if (response.getBody().equals("ok")){
                res = 1;
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return res;
    }

    @Override
    public List<Usuario> buscarPorEmail(String _email) {

        StrictMode.setThreadPolicy(GymApp.getPolicy());

        List<Usuario> usuarios = new ArrayList<>();

        Log.d("UsuarioDao", "buscarPorEmail::" + _email);
        Log.d("UsuarioDao", "wsUrl::" + wsUrl);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        try {
            String result = restTemplate.getForObject(wsUrl + "/findByEmail/" + _email, String.class);

            Map<String, Object> response = GymApp.getObjectMapper().readValue(result, HashMap.class);
            Usuario us = new Usuario();
            us.setEmail(response.get("email").toString());
            us.setNombre(response.get("nombre").toString());
            us.setApellido(response.get("apellido").toString());
            us.setEstatura(Integer.parseInt(response.get("estatura").toString()));
            us.setFechaNac(response.get("fechaNac").toString());
            us.setContrasegna(response.get("contrasegna").toString());

            usuarios.add(us);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch(Exception e){
            Log.d("Login", e.getLocalizedMessage());
        } finally {

        }

        return usuarios;
    }

}

