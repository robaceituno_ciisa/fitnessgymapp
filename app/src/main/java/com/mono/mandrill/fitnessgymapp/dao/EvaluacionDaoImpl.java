package com.mono.mandrill.fitnessgymapp.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mono.mandrill.fitnessgymapp.GymApp;
import com.mono.mandrill.fitnessgymapp.model.Evaluacion;
import com.mono.mandrill.fitnessgymapp.model.Usuario;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EvaluacionDaoImpl implements EvaluacionDao {

    String wsUrl = "http://" + GymApp.getServerHostname() + "/evaluations";

    @Override
    public long insertar( Evaluacion _evaluacion) {
        long res = 0;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();
        map.put("Content-Type", "application/json");

        headers.setAll(map);

        Map<String, Object> payload =new HashMap<>();

        payload.put("email", _evaluacion.getEmail());
        payload.put("fecha", _evaluacion.getFecha());
        payload.put("imc", _evaluacion.getImc());
        payload.put("peso", _evaluacion.getPeso());

        HttpEntity<?> request = null;
        try {
            request = new HttpEntity<>(GymApp.getObjectMapper().writeValueAsString(payload), headers);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            ResponseEntity<?> response = restTemplate.postForEntity(wsUrl, request, String.class);

            if (response.getBody().equals("ok")){
                res = 1;
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return res;
    }

    @Override
    public List<Evaluacion> buscarPorEmailEntreFechas( String _email, String _inicio, String _termino) {
        List<Evaluacion> _evas = new ArrayList<>();

        StrictMode.setThreadPolicy(GymApp.getPolicy());

        Log.d("Dao", _email + " - " + _inicio + " - " + _termino);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

        String result = restTemplate.getForObject(wsUrl + "/" + _email + "/" + _inicio + "/" + _termino, String.class);

        try {

            JSONArray lista = new JSONArray(result);

            for (int i=0; i<lista.length(); i++){
                JSONObject obj = lista.getJSONObject(i);
                Evaluacion ev = new Evaluacion();
                ev.setFecha(obj.getString("fecha"));
                ev.setEmail(obj.getString("email"));
                ev.setImc(BigDecimal.valueOf(obj.getDouble("imc")).floatValue());
                ev.setPeso(BigDecimal.valueOf(obj.getLong("peso")).floatValue());
                _evas.add(ev);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return _evas;
    }

    @Override
    public long borrarPorEmailFecha( String _email, String _fecha) {
        Log.d("Dao", _email + " - " + _fecha);

        long res = 0;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
        Map map = new HashMap<String, String>();
        map.put("Content-Type", "application/json");
        headers.setAll(map);

        Map<String, Object> payload =new HashMap<>();
        //payload.put("dummy", "");

        HttpEntity<String> entity = null;
        try {
            //entity = new HttpEntity<String>();

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            restTemplate.execute(wsUrl + "/" + _email + "/" + _fecha, HttpMethod.DELETE, null, null);

            res = 1;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
